import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:test_app/core/button.dart';

import '../widgets/header.dart';
import '../widgets/i_tab.dart';
import '../widgets/legend.dart';
import '../widgets/one_section.dart';
import '../widgets/three_section.dart';
import '../widgets/two_section.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int currentIndex = 0;
  setCurrentIndex(int index) {
    setState(() {
      currentIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(children: [
        Expanded(
            child: SingleChildScrollView(
                child: Column(children: [
          const Legend(),
          const SizedBox(
            height: 30,
          ),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: ITab(
              onChange: setCurrentIndex,
              tabs: const ["Arbeitnehmer", "Arbeitgeber", "Temporärbüro"],
            ),
          ),
          const SizedBox(
            height: 30,
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
            child: Text("Drei einfache Schritte zu deinem neuen Job",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 30, color: Color(0xFF4A5568))),
          ),
          Stack(
            alignment: Alignment.center,
            children: [
              ResponsiveBuilder(builder: (context, sizingInformation) {
                return Column(
                  children: [
                    SizedBox(
                      width: 800,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          OneSection(
                            text: _getCurrentTitleSectionOne(currentIndex),
                            image: _getCurrentImageSectionOne(currentIndex),
                            number: "1.",
                          ),
                          if (sizingInformation.deviceScreenType ==
                              DeviceScreenType.desktop)
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 180, top: 120),
                              child: SizedOverflowBox(
                                size: Size(200, 20),
                                child: Image.asset(
                                  'assets/images/arrow1.png',
                                  width: 340,
                                ),
                              ),
                            ),
                        ],
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.only(top: 10, bottom: 10),
                      child: Stack(
                        children: [
                          Positioned.fill(
                              child: Opacity(
                            opacity: 0.5,
                            child: SvgPicture.asset(
                              'assets/images/center_bg.svg',
                              alignment: Alignment.topCenter,
                              fit: BoxFit.fill,
                              height: 500,
                            ),
                          )),
                          TwoSection(
                            text: _getCurrentTitleSectionTwo(currentIndex),
                            image: _getCurrentImageSectionTwo(currentIndex),
                            number: "2.",
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 800,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (sizingInformation.deviceScreenType ==
                              DeviceScreenType.desktop)
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 180, top: 120),
                              child: SizedOverflowBox(
                                size: Size(150, 20),
                                child: Image.asset(
                                  'assets/images/arrow2.png',
                                  width: 340,
                                ),
                              ),
                            ),
                          ThreeSection(
                            text: _getCurrentTitleSectionTree(currentIndex),
                            image: _getCurrentImageSectionTree(currentIndex),
                            number: "3.",
                          )
                        ],
                      ),
                    )
                  ],
                );
              })
            ],
          )
        ]))),
        const Positioned(
          child: Header(),
          top: 0,
          left: 0,
          right: 0,
        )
      ]),
    );
  }
}

String _getCurrentTitleSectionOne(int i) {
  switch (i) {
    case 1:
      return "Erstellen dein Unternehmensprofil";
    case 2:
      return "Erstellen dein Unternehmensprofil";
    default:
      return "Erstellen dein Lebenslauf";
  }
}

String _getCurrentImageSectionOne(int i) {
  switch (i) {
    case 1:
      return "assets/images/undraw_Profile_data_re_v81r@2x.png";
    case 2:
      return "assets/images/undraw_Profile_data_re_v81r@2x.png";
    default:
      return "assets/images/undraw_Profile_data_re_v81r@2x.png";
  }
}

String _getCurrentTitleSectionTwo(int i) {
  switch (i) {
    case 1:
      return "Erstellen ein Jobinserat";
    case 2:
      return "Erhalte Vermittlungs- angebot von Arbeitgeber";
    default:
      return "Erstellen dein Lebenslauf";
  }
}

String _getCurrentImageSectionTwo(int i) {
  switch (i) {
    case 1:
      return "assets/images/undraw_about_me_wa29@2x.png";
    case 2:
      return "assets/images/undraw_job_offers_kw5d@2x.png";
    default:
      return "assets/images/undraw_task_31wc@2x.png";
  }
}

String _getCurrentTitleSectionTree(int i) {
  switch (i) {
    case 1:
      return "Wähle deinen neuen Mitarbeiter aus";
    case 2:
      return "Vermittlung nach Provision oder Stundenlohn";
    default:
      return "Mit nur einem Klick bewerben";
  }
}

String _getCurrentImageSectionTree(int i) {
  switch (i) {
    case 1:
      return "assets/images/undraw_swipe_profiles1_i6mr@2x.png";
    case 2:
      return "assets/images/undraw_business_deal_cpi9@2x.png";
    default:
      return "assets/images/undraw_personal_file_222m@2x.png";
  }
}
