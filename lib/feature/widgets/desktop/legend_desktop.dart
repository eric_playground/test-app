import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/button.dart';

class LegendDesktop extends StatelessWidget {
  const LegendDesktop({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(
          child: SvgPicture.asset(
            'assets/images/legend_bg.svg',
            alignment: Alignment.topCenter,
            height: 900,
            fit: BoxFit.fill,
          ),
        ),
        Container(
          height: MediaQuery.of(context).size.height * 0.7,
          padding: EdgeInsets.only(top: 100),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(
                  width: 400,
                  child: Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: const [
                        Text(
                          "Deine Job website",
                          textAlign: TextAlign.left,
                          style: TextStyle(fontSize: 50),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        SizedBox(child: Button(text: "Kostenlos Registrieren"))
                      ],
                    ),
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(600),
                  child: Container(
                    width: 600,
                    height: 600,
                    color: Colors.white,
                    child: Image.asset(
                      'assets/images/undraw_agreement_aajr@2x.png',
                      width: 600,
                    ),
                  ),
                ),
              ]),
        ),
      ],
    );
  }
}
