import 'package:flutter/material.dart';

class OneSectionDesktop extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const OneSectionDesktop({
    super.key,
    required this.text,
    required this.number,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Text(number,
                style: TextStyle(fontSize: 150, color: Color(0xFF718096))),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(bottom: 29),
                child: Container(
                  width: 250,
                  child: Text(
                    text,
                    style: TextStyle(fontSize: 20, color: Color(0xFF718096)),
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Image.asset(
              image,
              width: 300,
            ),
          ]),
    );
  }
}
