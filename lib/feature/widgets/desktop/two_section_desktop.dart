import 'package:flutter/material.dart';

class TwoSectionDesktop extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const TwoSectionDesktop({
    required this.text,
    required this.number,
    required this.image,
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 100),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Image.asset(
              image,
              width: 300,
            ),
            SizedBox(
              width: 80,
            ),
            Text(number,
                style: TextStyle(fontSize: 150, color: Color(0xFF718096))),
            Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Container(
                width: 250,
                child: Text(
                  text,
                  style: TextStyle(fontSize: 20, color: Color(0xFF718096)),
                ),
              ),
            ),
          ]),
    );
  }
}
