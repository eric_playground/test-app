import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

class ITab extends StatefulWidget {
  final List<String> tabs;
  final Function(int) onChange;
  const ITab({super.key, required this.tabs, required this.onChange});

  @override
  State<ITab> createState() => _ITabState();
}

class _ITabState extends State<ITab> {
  int currentIndex = 0;
  setCurrentIndex(int index) {
    setState(() {
      currentIndex = index;
      widget.onChange(index);
    });
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      return Container(
        width: double.infinity,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ...List.generate(widget.tabs.length, (index) {
              if (sizingInformation.deviceScreenType ==
                  DeviceScreenType.desktop)
                return Flexible(
                  child: Items(
                      index: index,
                      tabs: widget.tabs,
                      currentIndex: currentIndex,
                      setCurrentIndex: setCurrentIndex),
                );

              return Expanded(
                  child: Items(
                      index: index,
                      tabs: widget.tabs,
                      currentIndex: currentIndex,
                      setCurrentIndex: setCurrentIndex));
            })
          ],
        ),
      );
    });
  }
}

class Items extends StatelessWidget {
  final int index;
  final int currentIndex;
  final List<String> tabs;
  final Function(int) setCurrentIndex;
  const Items(
      {super.key,
      required this.index,
      required this.tabs,
      required this.currentIndex,
      required this.setCurrentIndex});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(index == 0 ? 10 : 0),
              bottomLeft: Radius.circular(index == 0 ? 10 : 0),
              topRight: Radius.circular(index == tabs.length - 1 ? 10 : 0),
              bottomRight: Radius.circular(index == tabs.length - 1 ? 10 : 0)),
          color: currentIndex == index ? Color(0xFF81E6D9) : Colors.white,
          border: Border.all(
              color: currentIndex != index ? Colors.grey : Colors.transparent)),
      child: InkWell(
          onTap: () {
            setCurrentIndex(index);
          },
          child: Text(tabs[index],
              textAlign: TextAlign.center,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                  color: currentIndex == index
                      ? Colors.white
                      : const Color(0xFF319795),
                  fontSize: 16))),
    );
  }
}
