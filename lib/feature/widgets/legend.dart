import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import '../../core/button.dart';
import 'desktop/legend_desktop.dart';
import 'mobile/legend_mobile.dart';

class Legend extends StatelessWidget {
  const Legend({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
        return LegendDesktop();
      }
      return LegendMobile();
    });
  }
}
