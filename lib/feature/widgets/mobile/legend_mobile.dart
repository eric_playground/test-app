import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../../../core/button.dart';

class LegendMobile extends StatelessWidget {
  const LegendMobile({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        SvgPicture.asset(
          'assets/images/legend_bg.svg',
          alignment: Alignment.topCenter,
          fit: BoxFit.fitHeight,
          height: 800,
        ),
        Container(
          color: Colors.transparent,
          padding: EdgeInsets.only(top: 100),
          child: Column(children: [
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Text(
                "Deine Job website",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 50),
              ),
            ),
            Image.asset('assets/images/undraw_agreement_aajr@2x.png'),
            SizedBox(
              height: 30,
            ),
            Container(
              padding: EdgeInsets.only(top: 30),
              decoration: BoxDecoration(
                borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 1,
                    blurRadius: 10,
                    offset: const Offset(0, -10), // changes position of shadow
                  ),
                ],
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0).copyWith(bottom: 80),
                child: Column(
                  children: [
                    SizedBox(child: Button(text: "Kostenlos Registrieren"))
                  ],
                ),
              ),
            ),
          ]),
        ),
      ],
    );
  }
}
