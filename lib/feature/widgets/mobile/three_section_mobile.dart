import 'package:flutter/material.dart';

class ThreeSectionMobile extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const ThreeSectionMobile({
    super.key,
    required this.text,
    required this.number,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(children: [
        Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 50),
                child: Text(number,
                    style: TextStyle(fontSize: 150, color: Color(0xFF718096))),
              ),
              Expanded(
                  child: Padding(
                padding: const EdgeInsets.only(bottom: 40),
                child: Text(
                  text,
                  style: TextStyle(fontSize: 20, color: Color(0xFF718096)),
                ),
              ))
            ],
          ),
        ),
        Image.asset(
          image,
          width: 300,
        ),
      ]),
    );
  }
}
