import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop/one_section_desktop.dart';
import 'mobile/one_section_mobile.dart';

class OneSection extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const OneSection({
    super.key,
    required this.text,
    required this.number,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
        return OneSectionDesktop(
            text: text, number: number, image: image
        );
      }
      return OneSectionMobile(
          text: text, number: number, image: image
      );
    });
  }
}
