import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop/three_section_desktop.dart';
import 'mobile/three_section_mobile.dart';

class ThreeSection extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const ThreeSection({
    super.key,
    required this.text,
    required this.number,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
        return ThreeSectionDesktop(
          text: text,
          number: number,
          image: image,
        );
      }
      return ThreeSectionMobile(
        text: text,
        number: number,
        image: image,
      );
    });
  }
}
