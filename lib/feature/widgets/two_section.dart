import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'desktop/one_section_desktop.dart';
import 'desktop/two_section_desktop.dart';
import 'mobile/one_section_mobile.dart';
import 'mobile/two_section_mobile.dart';

class TwoSection extends StatelessWidget {
  final String text;
  final String number;
  final String image;
  const TwoSection({
    super.key,
    required this.text,
    required this.number,
    required this.image,
  });

  @override
  Widget build(BuildContext context) {
    return ResponsiveBuilder(builder: (context, sizingInformation) {
      if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
        return TwoSectionDesktop(text: text, number: number, image: image);
      }
      return TwoSectionMobile(text: text, number: number, image: image);
    });
  }
}
